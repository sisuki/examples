        subroutine READ_Cor_P_S
        INCLUDE 'COMMONzt.FOR'
        integer :: ie, j, be, m
        real, allocatable :: matrix(:, :) !matrix(n, m)
      real sigma_r,sigma_ksi, sigma_pulse
      integer :: i, n, clock, read_file
      integer, dimension(:), allocatable :: seed
      real, dimension(:), allocatable :: XY_rand
      sigma_r=0.25
      sigma_pulse=0.8
      sigma_ksi=0.01

        open(file = "BEAM1007.DAT", unit = 101)

        be = 10
        m = 99416
!iostat:
! = -1 error: end of file
! = -2 error: end of record


        allocate( matrix(be, m) )

      do ie=1, m
         read (101, *) (matrix (j, ie), j=1,be)
      end do

      call random_seed(size = n)
      allocate(seed(n))

      call system_clock(count=clock)!

      seed = clock + 37*(/ (i - 1, i = 1, n) /)
      call random_seed(put = seed)

      deallocate(seed)
      J=6*MAXNCELL
      allocate(XY_rand(J))
      call random_number(XY_rand)
      !write(777,*)seed, XY_rand
      do NOM=1,MAXNCELL
      if (NOM.lt.m) then
      SZs(NMAXNCELL)=matrix(8,NOM)!0.9
      SYs(NMAXNCELL)=matrix(10,NOM)!-0.33456!0!SQRT(1-0.9**2)/2**(0.5)!
      SXs(NMAXNCELL)=matrix(9,NOM)!SQRT(1-SYs(NMAXNCELL)**2-SZs(NMAXNCELL)**2)
C
      Xs(NMAXNCELL)=matrix(4,NOM)!sigma_r*sqrt(-2*log(XY_rand(6*NOM-3)))
    ! **cos(2*3.14159*XY_rand(6*NOM-1))!0.1
      Ys(NMAXNCELL)=matrix(6,NOM)!sigma_r*sqrt(-2*log(XY_rand(6*NOM-3)))
   !  **sin(2*3.14159*XY_rand(6*NOM-1))!sqrt(0.25**2-Xs(NOM)**2)!
      Tloc(NMAXNCELL)=matrix(2,NOM)!sigma_ksi*sqrt(-2*log(XY_rand(6*NOM-4)))
    ! **sin(2*3.14159*XY_rand(6*NOM-5))+
    ! *3.2
      PX(NMAXNCELL)=matrix(5,NOM)
      PY(NMAXNCELL)=matrix(7,NOM)
      PZ(NMAXNCELL)=matrix(3,NOM)
      else
      SZs(NMAXNCELL)=0.9
      SYs(NMAXNCELL)=-0.33456!0!SQRT(1-0.9**2)/2**(0.5)!
      SXs(NMAXNCELL)=SQRT(1-SYs(NMAXNCELL)**2-SZs(NMAXNCELL)**2)
      PX(NMAXNCELL)=sigma_pulse*sqrt(-2*log(XY_rand(6*NOM)))
     **sin(2*3.14159*XY_rand(6*NOM-2))
      PY(NMAXNCELL)=sigma_pulse*sqrt(-2*log(XY_rand(6*NOM)))
     **cos(2*3.14159*XY_rand(6*NOM-2))
      PZ(NMAXNCELL)=132.0

c===========random array==================================

      Xs(NMAXNCELL)=10
      Ys(NMAXNCELL)=10
      Tloc(NMAXNCELL)=!sigma_ksi*sqrt(-2*log(XY_rand(6*NOM-4)))
    ! **sin(2*3.14159*XY_rand(6*NOM-5))+
     *3.2
C
      !if ((Xs(NOM)**2+Ys(NOM)**2)>0.25) then
      !  SZs(NOM)=0
      !  SXs(NOM)=0
      !  SYs(NOM)=0
      !end if
      end if
C
      Xs0=Xs(NMAXNCELL)
      Ys0=Ys(NMAXNCELL)
      SFIs(NMAXNCELL)=(-SXs(NMAXNCELL)*Ys(NMAXNCELL)
     *+SYs(NMAXNCELL)*Xs(NMAXNCELL))
     */sqrt(Xs(NMAXNCELL)**2+Ys(NMAXNCELL)**2)
      SRs(NMAXNCELL)=(SXs(NMAXNCELL)*Xs(NMAXNCELL)
     *+SYs(NMAXNCELL)*Ys(NMAXNCELL))
     */sqrt(Xs(NMAXNCELL)**2+Ys(NMAXNCELL)**2)
C===========init middle===================================
      Xsp(NMAXNCELL)=Xs(NMAXNCELL)
      Ysp(NMAXNCELL)=Ys(NMAXNCELL)
      Tlocp(NMAXNCELL)=Tloc(NMAXNCELL)
C
      PXp(NMAXNCELL)=PX(NMAXNCELL)
      PYp(NMAXNCELL)=PY(NMAXNCELL)
      PZp(NMAXNCELL)=PZ(NMAXNCELL)
C
      SXsp(NMAXNCELL)=SXs(NMAXNCELL)
      SYsp(NMAXNCELL)=SYs(NMAXNCELL)
      SZsp(NMAXNCELL)=SZs(NMAXNCELL)
    !  write(777,*)Xs(NOM),Ys(NOM)
    !  IF (sqrt(Xs(NMAXNCELL)**2+Ys(NMAXNCELL)**2).lt.(0.5))
      NMAXNCELL=NMAXNCELL+1
      end do
      NMAXNCELL=NMAXNCELL-1
      deallocate(XY_rand)
!=============================initial coordinats froms previous stage=====

!      do NOM=1,MAXNCELL

!      if (NOM.gt.99710) then
!      SZs(NMAXNCELL)=0
!      SYs(NMAXNCELL)=0
!      SXs(NMAXNCELL)=1
C
!      Xs(NMAXNCELL)=10
!      Ys(NMAXNCELL)=10
!      Tloc(NMAXNCELL)=2.8
C

!      PX(NMAXNCELL)=0
!      PY(NMAXNCELL)=0
!      PZ(NMAXNCELL)=0
C
!      else
!      SZs(NMAXNCELL)=matrix(8,NOM)
!      SYs(NMAXNCELL)=matrix(10,NOM)
!      SXs(NMAXNCELL)=matrix(9,NOM)
C
!      Xs(NMAXNCELL)=matrix(4,NOM)
!      Ys(NMAXNCELL)=matrix(6,NOM)
!      Tloc(NMAXNCELL)=matrix(2,NOM)
C

!      PX(NMAXNCELL)=matrix(5,NOM)
!      PY(NMAXNCELL)=matrix(7,NOM)
!      PZ(NMAXNCELL)=matrix(3,NOM)
C
!      Xs0=Xs(NMAXNCELL)
!      Ys0=Ys(NMAXNCELL)
!      SFIs(NMAXNCELL)=(-SXs(NMAXNCELL)*Ys(NMAXNCELL)
!     *+SYs(NMAXNCELL)*Xs(NMAXNCELL))
!     */sqrt(Xs(NMAXNCELL)**2+Ys(NMAXNCELL)**2)
!      SRs(NMAXNCELL)=(SXs(NMAXNCELL)*Xs(NMAXNCELL)
!     *+SYs(NMAXNCELL)*Ys(NMAXNCELL))
!     */sqrt(Xs(NMAXNCELL)**2+Ys(NMAXNCELL)**2)
C===========init middle===================================
!      Xsp(NMAXNCELL)=Xs(NMAXNCELL)
!      Ysp(NMAXNCELL)=Ys(NMAXNCELL)
!      Tlocp(NMAXNCELL)=Tloc(NMAXNCELL)
C
!      PXp(NMAXNCELL)=PX(NMAXNCELL)
!      PYp(NMAXNCELL)=PY(NMAXNCELL)
!      PZp(NMAXNCELL)=PZ(NMAXNCELL)
C
!      SXsp(NMAXNCELL)=SXs(NMAXNCELL)
!      SYsp(NMAXNCELL)=SYs(NMAXNCELL)
!      SZsp(NMAXNCELL)=SZs(NMAXNCELL)
!      end if
!      NMAXNCELL=NMAXNCELL+1
!      end do
!      NMAXNCELL=NMAXNCELL-1
!matrix is read from the file
close(101)

!        do i = 1, n

!        do j = 1, m
!        write(2,*) matrix(i, j)
!        end do
!       end do

!make the memory free
        deallocate(matrix)
        close(101)
       end
