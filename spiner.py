import glob
import numpy
beams=glob.glob('/home/laplac_mod/RESULTS/BEAM*.DAT')
beams.sort()
beam_p=open('/home/sisuki/laplac_mod/SPINER.DAT','w')
beam_p.write('  Pz    Pol\n')
s=[]
#p_full=[]
pulse=[]
#pulse_avg=[]
s_0=numpy.array([0.9,0.27941,-0.33456])
#s_0=numpy.array([0.9,0.43588989,0])
#s_0=numpy.array([0.9,0,0.43588989])
#s_0=numpy.array([0.9,-(1-0.9**2)**(0.5)/2**0.5,(1-0.9**2)**(0.5)/2**0.5])
for i in range(len(beams)):
    s=[]
    pulse=[]
    print i
    data=open(beams[i],'r').readlines()
    data=[elem.split('\n') for elem in data[1:]]
    for j in range(len(data)):
        a=data[j][0].split(' ')
        a=[float(elem) for elem in a if elem]
        b=[elem for elem in a[-3:]]
        s.append(b)
        pulse.append((a[2]**2+a[4]**2+a[6]**2+1)**(0.5))
    #print a
    s_np=numpy.array(s)
    #print s_np
    pulse_np=numpy.array(pulse)
    #print pulse_np
    #print s_delta
    p_full=numpy.sum(s_np,axis=0)/len(data)
    s_delta=(p_full-s_0)                                      
    p_dfull=(-1)*(numpy.sum(s_delta*s_delta))**(0.5)
    #print pd_full
    pulse_avg=numpy.sum(pulse_np)/len(data)*0.511/1000
    output=str(pulse_avg)+'   '+str(p_dfull)+'\n'
    beam_p.write(output)
beam_p.close()
